# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/microros/microros_ws/firmware/mcu_ws/uros/rosidl_typesupport_microxrcedds/rosidl_typesupport_microxrcedds_c/src/deserialize_buffer_utility.c" "/home/microros/microros_ws/firmware/mcu_ws/build/rosidl_typesupport_microxrcedds_c/CMakeFiles/rosidl_typesupport_microxrcedds_c.dir/src/deserialize_buffer_utility.c.obj"
  "/home/microros/microros_ws/firmware/mcu_ws/uros/rosidl_typesupport_microxrcedds/rosidl_typesupport_microxrcedds_c/src/identifier.c" "/home/microros/microros_ws/firmware/mcu_ws/build/rosidl_typesupport_microxrcedds_c/CMakeFiles/rosidl_typesupport_microxrcedds_c.dir/src/identifier.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/home/microros/microros_ws/firmware/mcu_ws/uros/rosidl_typesupport_microxrcedds/rosidl_typesupport_microxrcedds_c/include"
  "/home/microros/microros_ws/firmware/NuttX/include"
  "/home/microros/microros_ws/firmware/NuttX/include/cxx"
  "/home/microros/microros_ws/firmware/NuttX/include/uClibc++"
  "/home/microros/microros_ws/firmware/mcu_ws/install/rosidl_runtime_c/include"
  "/home/microros/microros_ws/firmware/mcu_ws/install/rosidl_typesupport_interface/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
