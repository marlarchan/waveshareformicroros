# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/microros/microros_ws/firmware/mcu_ws/uros/tracetools/tracetools/src/tracetools.c" "/home/microros/microros_ws/firmware/mcu_ws/build/tracetools/CMakeFiles/tracetools.dir/src/tracetools.c.obj"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "ROS_PACKAGE_NAME=\"tracetools\""
  "tracetools_VERSION=\"1.0.2\""
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "/home/microros/microros_ws/firmware/NuttX/include"
  "/home/microros/microros_ws/firmware/NuttX/include/cxx"
  "/home/microros/microros_ws/firmware/NuttX/include/uClibc++"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/microros/microros_ws/firmware/mcu_ws/uros/tracetools/tracetools/src/utils.cpp" "/home/microros/microros_ws/firmware/mcu_ws/build/tracetools/CMakeFiles/tracetools.dir/src/utils.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "ROS_PACKAGE_NAME=\"tracetools\""
  "tracetools_VERSION=\"1.0.2\""
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  "/home/microros/microros_ws/firmware/NuttX/include"
  "/home/microros/microros_ws/firmware/NuttX/include/cxx"
  "/home/microros/microros_ws/firmware/NuttX/include/uClibc++"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
