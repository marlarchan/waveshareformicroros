set(CMAKE_HOST_SYSTEM "Linux-5.4.0-47-generic")
set(CMAKE_HOST_SYSTEM_NAME "Linux")
set(CMAKE_HOST_SYSTEM_VERSION "5.4.0-47-generic")
set(CMAKE_HOST_SYSTEM_PROCESSOR "x86_64")

include("/home/microros/microros_ws/firmware/apps/uros/arm_toolchain.cmake")

set(CMAKE_SYSTEM "Generic")
set(CMAKE_SYSTEM_NAME "Generic")
set(CMAKE_SYSTEM_VERSION "")
set(CMAKE_SYSTEM_PROCESSOR "arm")

set(CMAKE_CROSSCOMPILING "1")

set(CMAKE_SYSTEM_LOADED 1)
