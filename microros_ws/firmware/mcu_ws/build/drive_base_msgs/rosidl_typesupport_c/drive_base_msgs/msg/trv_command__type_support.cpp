// generated from rosidl_typesupport_c/resource/idl__type_support.cpp.em
// with input from drive_base_msgs:msg/TRVCommand.idl
// generated code does not contain a copyright notice

#include "cstddef"
#include "rosidl_runtime_c/message_type_support_struct.h"
#include "drive_base_msgs/msg/rosidl_typesupport_c__visibility_control.h"
#include "drive_base_msgs/msg/detail/trv_command__struct.h"
#include "rosidl_typesupport_c/visibility_control.h"
#include "drive_base_msgs/msg/detail/trv_command__rosidl_typesupport_microxrcedds_c.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_C_EXPORT_drive_base_msgs
const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_c, drive_base_msgs, msg, TRVCommand)() {
  return ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_microxrcedds_c, drive_base_msgs, msg, TRVCommand)();
}

#ifdef __cplusplus
}
#endif
